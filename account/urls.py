from django.urls import path
from knox import views as knox_views

from account.views import RegistrationView, MentorRegistrationView, LoginAPI

urlpatterns = [
    path('register/', RegistrationView.as_view(), name='register'),
    path('mentor_register/', MentorRegistrationView.as_view(), name='mentor_register'),
    path('login/', LoginAPI.as_view(), name='login'),
    path('logout/', knox_views.LogoutView.as_view(), name='logout'),
]

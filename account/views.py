from django.shortcuts import render
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from django.contrib.auth import login

from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.views import LoginView as KnoxLoginView
from knox.models import AuthToken
from account.serializers import RegistrationSerializer, MentorRegistrationSerializer


class LoginAPI(KnoxLoginView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPI, self).post(request, format=None)


class RegistrationView(APIView):
    serializer_class = RegistrationSerializer

    def get(self, request, *args, **kwargs):
        return Response({'Hello': 'Here the form for registration.'})

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        data = {}
        if serializer.is_valid():
            user = serializer.save()
            data['response'] = 'successfully registered a new user'
            data['email'] = user.email
            data['username'] = user.username
            token = AuthToken.objects.create(user)[1]
            data['token'] = token
        else:
            data = serializer.errors
        return Response(data)


class MentorRegistrationView(APIView):

    def post(self, request, *args, **kwargs):
        data = {}
        serializer = MentorRegistrationSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            data['response'] = 'successfully registered a new mentor'
            data['email'] = user.email
            data['username'] = user.username
            token = AuthToken.objects.get(user=user).key
            data['token'] = token
        else:
            data = serializer.errors
        return Response(data)






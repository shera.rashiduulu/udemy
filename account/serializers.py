from rest_framework import serializers
from webapp.models import User, Profile


class RegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'password2']
        extra_kwargs = {'password': {'write_only': True}}

    def save(self, **kwargs):
        user = User(email=self.validated_data['email'],
                    username=self.validated_data['username'],
                    )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError({'password': 'Password must match'})
        user.set_password(password)
        user.save()
        return user


class MentorRegistrationSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    audition = serializers.CharField()
    type_of_teaching = serializers.CharField()

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'password2', 'audition', 'type_of_teaching']
        extra_kwargs = {'password': {'write_only': True}}
        # l

    def save(self, **kwargs):
        user = User(email=self.validated_data['email'],
                    username=self.validated_data['username'],
                    )
        password = self.validated_data['password']
        password2 = self.validated_data['password2']

        if password != password2:
            raise serializers.ValidationError({'password': 'Password must match'})
        user.set_password(password)
        user.is_mentor = True
        user.save()
        profile = Profile.objects.create(user=user)
        profile.audition = self.validated_data['audition']
        profile.type_of_teaching = self.validated_data['type_of_teaching']
        profile.save()
        return user

from django.urls import path

from webapp.views import UserViewSet, ProfileViewSet, UserCoursesViewSet, FavoritesViewSet, PaymentViewSet, CommentsViewSet, RatingsViewSet

urlpatterns = [
    path('users/', UserViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('user/<int:pk>/', UserViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('profiles/', ProfileViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('profile/<int:pk>/', ProfileViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('users_courses/', UserCoursesViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('users_course/<int:pk>/', UserCoursesViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('favorites/', FavoritesViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('favorite/<int:pk>/', FavoritesViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('payments/', PaymentViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('payment/<int:pk>/', PaymentViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('comments/', CommentsViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('comment/<int:pk>/', CommentsViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('ratings/', RatingsViewSet.as_view({'get': 'list', 'post': 'create'})),
    path('rating/<int:pk>/', RatingsViewSet.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),

]
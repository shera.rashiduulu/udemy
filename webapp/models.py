from datetime import datetime
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import AbstractUser
from knox.models import AuthToken


class User(AbstractUser, BaseUserManager):
    email = models.EmailField(max_length=100, blank=True, null=True, unique=True)
    full_name = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    is_mentor = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return f"{self.email}"


AUDITION = [
        ('NOT', 'В настоящий момент нет'),
        ('SMALL', 'У меня маленькая аудитория'),
        ('BIG', 'У меня достаточная аудитория'),
    ]

TYPE = [
        ('privately', 'Лично, частным образом'),
        ('professionally', 'Лично, профессионально'),
        ('online', 'Онлайн'),
        ('other', 'Другое'),
    ]


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    type_of_teaching = models.CharField(max_length=50, choices=TYPE, default=TYPE[0])
    audition = models.CharField(max_length=50, choices=AUDITION, default=AUDITION[0])

    def __str__(self):
        return self.user.email


class Course(models.Model):
    name = models.CharField(max_length=100)
    price = models.FloatField()
    author = models.CharField(max_length=100)
    discount = models.IntegerField()
    description = models.TextField()


class Payment(models.Model):
    status = models.CharField(max_length=50)
    created_at = models.DateTimeField(default=datetime.now, blank=True)
    user = models.ForeignKey('User', on_delete=models.CASCADE, related_name='user_payment')
    amount = models.IntegerField()


class UserCourses(models.Model):
    user_id = models.ForeignKey('User', on_delete=models.CASCADE, related_name='user_course')
    course_id = models.ForeignKey('Course', on_delete=models.CASCADE, related_name='user_course')


class Favorite(models.Model):
    name = models.CharField(max_length=100)
    user_id = models.ForeignKey('User', on_delete=models.CASCADE, related_name='user_favorite')
    course_id = models.ForeignKey('Course', on_delete=models.CASCADE, related_name='user_favorite')


class Ratings(models.Model):
    rating_number = models.IntegerField()
    course_id = models.ForeignKey('Course', on_delete=models.CASCADE, related_name='course_rating')
    user_id = models.ForeignKey('User', on_delete=models.CASCADE, related_name='course_rating')


class Comments(models.Model):
    text = models.TextField(max_length=2000)
    author = models.ForeignKey('User', related_name='user_comment', on_delete=models.CASCADE)
    course = models.ForeignKey('Course', related_name='course_comment', on_delete=models.CASCADE)
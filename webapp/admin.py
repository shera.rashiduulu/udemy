from django.contrib import admin

from webapp.models import User, Payment, Ratings, Favorite, Comments, UserCourses, Course, Profile

admin.site.register(User)
admin.site.register(Payment)
admin.site.register(Ratings)
admin.site.register(Favorite)
admin.site.register(Comments)
admin.site.register(UserCourses)
admin.site.register(Course)
admin.site.register(Profile)

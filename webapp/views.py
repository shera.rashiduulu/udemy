from django.shortcuts import render
from rest_framework import viewsets

from webapp.models import User, Profile, Payment, Course, Ratings, Comments, UserCourses, Favorite
from webapp.serializers import UserSerializer,\
    ProfileSerializer, PaymentSerializer, CourseSerializer, \
    RatingsSerializer, CommentsSerializer, FavoriteSerializer, UserCoursesSerializer


class UserViewSet(viewsets.ModelViewSet):
    """Список пользователей"""
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    """Список профилей"""
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer


class PaymentViewSet(viewsets.ModelViewSet):
    """Список оплаты"""
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class CourseViewSet(viewsets.ModelViewSet):
    """Список курсов"""
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class RatingsViewSet(viewsets.ModelViewSet):
    """Список рейтингов"""
    queryset = Ratings.objects.all()
    serializer_class = RatingsSerializer


class UserCoursesViewSet(viewsets.ModelViewSet):
    """Список пользователь-курсов"""
    queryset = UserCourses.objects.all()
    serializer_class = UserCoursesSerializer


class CommentsViewSet(viewsets.ModelViewSet):
    """Список комментарий"""
    queryset = Comments.objects.all()
    serializer_class = CommentsSerializer


class FavoritesViewSet(viewsets.ModelViewSet):
    """Список избанных"""
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
